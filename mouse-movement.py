from random import randrange
from shutil import move
import pyautogui
import argparse
import datetime

pyautogui.FAILSAFE = False

def main():
    
    parser = argparse.ArgumentParser(description='Processing', add_help=False)
    parser.add_argument('-t', '--time', nargs='?', const=None, help='TIME', required = False)
    parser.add_argument('-m', '--minute', nargs='?', const='yes', help='MINUTE', required = False)
    parser.add_argument('-hr', '--hour', nargs='?', const='yes', help='HOUR', required = False)
    parser.add_argument('-h','--help', nargs='?', const='yes', help='HELP', required = False)

    args = parser.parse_args()

    time = args.time
    minute = args.minute
    hour = args.hour
    help = args.help

    screenSize = pyautogui.size()
    height = screenSize.height
    width = screenSize.width

    def move_mouse(tm):
        if tm:
            while tm:
                timer = str(datetime.timedelta(seconds=tm))
                print('Timer ativo: ', timer, end="\r")
                y = randrange(height)
                x = randrange(width)
                pyautogui.moveTo(x, y, 0.59)
                pyautogui.press('shift')
                tm -= 1
        else:
            while True:
                y = randrange(height)
                x = randrange(width)
                pyautogui.moveTo(x, y, 0.59)
                pyautogui.press('shift')


    def need_help():
        file = open('help.txt', 'r')
        file_content = file.read()
        print(file_content)
        file.close()


    if time is None and hour is None and minute is None:
        t_parm = False

    elif ((time is not None) and ((hour is None) or (minute is None))):
        if minute is None and hour is not None:
            if (str(hour.lower()) == 'y' or str(hour.lower()) == 'yes') and (minute is None):
                t_parm = (int(time) * 60) * 60
        elif hour is None and minute is not None:
            if (hour is None) and (str(minute.lower()) == 'y' or str(minute.lower()) == 'yes'):
                t_parm = int(time) * 60
        else:
            t_parm = int(time)

    elif time is None:
        if (hour.isnumeric() and minute.isnumeric()):
            horas = int(hour) * 3600
            minutos = int(minute) * 60
            t_parm = (horas + minutos)
        elif (hour.isnumeric() and minute is None):
            t_parm = (int(time) * 60) * 60
        elif (hour is None and minute.isnumeric()):
            t_parm = int(time) * 60

    try:
        if help == 'yes':
            need_help()
        else:
            move_mouse(int(t_parm))
            print('Processo encerrado, obrigado por usar o Mouse Movement PY!')
    except KeyboardInterrupt:
        print('Processo encerrado, obrigado por usar o Mouse Movement PY!')
    
if __name__ == '__main__':
    main()
    