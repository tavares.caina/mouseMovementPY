# Never Sleep Mouse Python #
Bot developed in Python, which is responsible for moving the mouse around and doesn't allow OS enter in rest mode.

## Instalation ##

- Python 3.x + 
- pip install pyautogui

## Usage ##
python .\mouse-movement.py [-t] [--time] X [-m] [--minute] Y [-hr] [--hour] Z

### Parameters: ###
-t --time: Time definition for timer countdown, if [MINUTE] or [HOUR] is not defined, default unit is seconds.

-m --minute: Unit of time for timer, if declared then the time passed in [TIME] is changed to minutes.

-hr --hour: Unit of time for timer, if declared then the time passed in [TIME] is changed to hours.

-h --help: Show help message and exit

## Useful Tips ##
- You can call execution without any parameter.This way will make the bot run until it is canceled by user (ctrl +c).
- Whe using [HOUR] and [MINUTE] the [TIME] must not be declared.

## Sample Usage: ##
- python .\mouse-movement.py -t 1 -m

    `1 minute countdown timer`

- python .\mouse-movement.py -t 1 -hr

    `1 hour countdown timer`

- python .\mouse-movement.py -m 1 -hr 30

    `1 hour and 30 minutes countdown timer`